"use strict";

module.exports = function( grunt ) {

  grunt.initConfig({

    copy: {
      src: {
        files: [
          { expand: true, cwd: "src/images", src: [ "**" ], dest: "tmp" },
          { expand: true, cwd: "src", src: [ "*.html" ], dest: "tmp" }
        ]
      },
      tmp: {
        files: [
          { expand: true, cwd: "tmp", src: [ "*.html" ], dest: "dist" },
        ]
      }
    },

    connect: {
      options: {
        port: "8080",
        useAvailablePort: true,
        livereload: true,
        open: true
      },
      dev: {
        options: {
          base: "dist/"
        }
      }
    },

    watch: {
      src: {
        files: [ "src/css/**/*.{scss}", "src/images/**/*.{gif,png,jpg,jpeg}", "src/**/*.html" ],
        tasks: [ "build:development" ],
        options: {
          livereload: true
        }
      }
    },

    compass: {
      dist: {
        options: {
	        sassDir: 'src/scss',
	        cssDir: 'tmp',
	        environment: 'production'
        }
      }
    },

    imagemin: {
      dist: {
        files: [
          { expand: true, cwd: "tmp/images", src: [ "**/*.{png,jpg,jpeg}" ], dest: "dist/images" }
        ]
      }
    },

    uncss: {
      dist: {
        files: {
          "tmp/core.css": [ "tmp/index.html" ]
        }
      }
    },

    htmlmin: {
      dist: {
        options: {
          removeComments: true
        },
        files: {
          "dist/index.html": "dist/index.html"
        }
      }
    },

	inline: {
	  dist: {
		  src: [ 'tmp/index.html' ]
	  }
	}

  });

  grunt.registerTask( "start", "Compiles the development environment", [
    "build:distribution",
    "connect:dev",
    "watch:src"
  ]);

  grunt.registerTask( "build:development", "Compiles the development build", [
    "copy:src",
    "compass",
    "copy:tmp"
  ]);

  grunt.registerTask( "build:distribution", "Compiles the distribution build", [
	"copy:src",
	"compass",
	"imagemin:dist",
	//"uncss:dist",
	"inline:dist",
    "htmlmin:dist",
	"copy:tmp"
  ]);


  grunt.loadNpmTasks( "grunt-contrib-copy" );
  grunt.loadNpmTasks( "grunt-contrib-connect" );
  grunt.loadNpmTasks( "grunt-contrib-watch" );
  grunt.loadNpmTasks( "grunt-contrib-imagemin" );
  grunt.loadNpmTasks( "grunt-uncss" );
  grunt.loadNpmTasks( "grunt-contrib-compass" );
  grunt.loadNpmTasks( "grunt-inline" );
  grunt.loadNpmTasks( "grunt-contrib-htmlmin" );


};
